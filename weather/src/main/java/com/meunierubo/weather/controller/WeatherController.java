package com.meunierubo.weather.controller;

import com.meunierubo.weather.business.WeatherBusiness;
import com.meunierubo.weather.entity.PrevisionEntity;
import jakarta.inject.Inject;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Controller;

import java.util.concurrent.ExecutionException;

import static com.meunierubo.weather.mapper.WeatherMapper.toEntity;
import static com.meunierubo.weather.mapper.WeatherMapper.todto;

@Controller
@Path("/previsions")
public class WeatherController {
    @Inject()
    private WeatherBusiness weatherBusiness;
    @Path("/{city}")
    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response previsions(@HeaderParam("user") String userAgent, @PathParam("city") String city) throws ExecutionException {
    if(userAgent.equals("Admin")){
        return Response.ok(todto(weatherBusiness.getCityWeatherAdmin(city))).build();
    }

    try {
        return Response.ok(todto(weatherBusiness.getCityWeather(city))).build();
    } catch (EmptyResultDataAccessException e) {
        return Response.ok("error 401").build();
    }

    }

    @Path("/{city}/t")
    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response previsions2(@PathParam("city") String city){
        return Response.ok(weatherBusiness.getCityWeather2(city)).build();
    }
}
