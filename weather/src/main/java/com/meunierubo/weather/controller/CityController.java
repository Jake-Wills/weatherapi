package com.meunierubo.weather.controller;


import com.meunierubo.weather.business.CityBusiness;
import com.meunierubo.weather.filter.AuthentificationRequired;
import dto.weatherapi.City;
import jakarta.inject.Inject;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.concurrent.ExecutionException;

import static com.meunierubo.weather.mapper.CityMapper.toEntity;


@Controller
@Path("/cities")
public class CityController {

    @Inject
    private CityBusiness cityBusiness;

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @AuthentificationRequired
    public Response createCity(City city) {
        this.cityBusiness.createCity(toEntity(city));
        return Response.ok().build();

    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{id}")
    public Response updateCity(@PathParam("id") String id,City city) {
        this.cityBusiness.updateCity(id,toEntity(city));
        return Response.ok().build();

    }

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCities() {
        return Response.ok(this.cityBusiness.getCities()).build();
    }

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{id}")
    public Response getCity(@PathParam("id") String id) throws ExecutionException {
        return Response.ok(this.cityBusiness.getCity(id)).build();
    }

    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{id}")
    public Response deleteCity(@PathParam("id") String id) {
        this.cityBusiness.deleteCity(id);
        return Response.ok().build();
    }


}
