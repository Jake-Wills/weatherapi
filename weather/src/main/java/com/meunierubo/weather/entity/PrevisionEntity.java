package com.meunierubo.weather.entity;

import lombok.Data;

import java.time.Instant;

@Data
public class PrevisionEntity {

    private Instant date;
    private String city;
    private float temperature;

    private EnumPrevisionState state;

}
