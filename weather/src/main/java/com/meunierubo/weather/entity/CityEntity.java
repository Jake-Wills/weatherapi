package com.meunierubo.weather.entity;

import lombok.Data;
import org.springframework.stereotype.Component;

@Component
@Data
public class CityEntity {

    private String id;
    private String region;
    private String country;
    private String postcode;
    private String name;
}
