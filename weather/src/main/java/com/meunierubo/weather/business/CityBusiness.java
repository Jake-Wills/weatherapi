package com.meunierubo.weather.business;

import com.meunierubo.weather.exception.FunctionalException;
import dto.weatherapi.City;
import com.meunierubo.weather.entity.CityEntity;
import com.meunierubo.weather.mapper.CityMapper;
import com.meunierubo.weather.repository.CityRepository;
import jakarta.inject.Inject;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.concurrent.ExecutionException;

@Component
public class CityBusiness {

    @Inject
    private CityRepository cityRepository;

    public void createCity(CityEntity city) {

        if(city.getId() !=null){
            cityRepository.createCity(CityMapper.toDto(city));
        }
        throw new FunctionalException("ID_NULLE","L\'ID de la city ne doit pas être nulle");
    }

    public void deleteCity(String id) {
        cityRepository.deleteCity(id);
    }

    public void updateCity(String id, CityEntity city) {
        cityRepository.updateCity(id, CityMapper.toDto(city));
    }

    public List<City> getCities() {
        return cityRepository.getCities();
    }

    public Object getCity(String id) throws EmptyResultDataAccessException {
        return cityRepository.getCity(id);
    }
}
