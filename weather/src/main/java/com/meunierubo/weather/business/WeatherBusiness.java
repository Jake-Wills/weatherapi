package com.meunierubo.weather.business;

import com.meunierubo.weather.entity.PrevisionEntity;
import com.meunierubo.weather.repository.CityRepository;
import com.meunierubo.weather.repository.WeatherRepository;
import jakarta.inject.Inject;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.QueryParam;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Component;

import java.util.concurrent.ExecutionException;

import static com.meunierubo.weather.mapper.WeatherMapper.toEntity;

@Component
public class WeatherBusiness {

    @Inject()
    private WeatherRepository weatherRepository;

    @Inject()
    private CityRepository cityRepository;

    public PrevisionEntity getCityWeather(String city) throws EmptyResultDataAccessException{
            Object cityr = cityRepository.getCity(city);
            return toEntity(weatherRepository.getCityWeather(city));

    }
    public PrevisionEntity getCityWeatherAdmin(String city) {
        return toEntity(weatherRepository.getCityWeather(city));

    }
    public Object getCityWeather2(String city){
        return weatherRepository.getCityWeather(city);
    }
}
