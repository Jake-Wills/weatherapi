package com.meunierubo.weather.exception;

import jakarta.ws.rs.ext.ExceptionMapper;

public class FunctionalException extends RuntimeException{
private String description;
private String code;

public FunctionalException(String code, String description){
    super(description);
    this.code = code;
}

public String getDescription(){
    return this.description;
}

    public String getCode(){
        return this.code;
    }
    public void setDescription(String description){
        this.description = description;
    }

    public void setCode(String code){
        this.code = code;
    }

}
