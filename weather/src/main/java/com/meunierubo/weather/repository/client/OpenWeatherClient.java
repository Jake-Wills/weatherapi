package com.meunierubo.weather.repository.client;

import dto.openWeatherClient.Model200;
import feign.Param;
import feign.RequestLine;
import org.springframework.cache.annotation.Cacheable;

public interface OpenWeatherClient {
    //@Cacheable(value = "weather", key ="#cityName")
    @RequestLine("GET data/2.5/weather?q={cityName}&appid={token}&units=metric")
    Model200 getWeather(@Param("cityName")String cityName, @Param("token")String token);
}
