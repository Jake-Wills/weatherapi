package com.meunierubo.weather.repository;

import com.meunierubo.weather.repository.client.OpenWeatherClient;
import dto.openWeatherClient.Model200;
import dto.weatherapi.Prevision;
import jakarta.inject.Inject;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;

@Controller
public class WeatherRepository {
    @Inject
    private OpenWeatherClient openWeatherClient;

    @Value("${spring.secrets.openweather.token}")
    private String token;
    public Model200 getCityWeather(String cityName){
        return  openWeatherClient.getWeather(cityName, token);
    }
    
}
