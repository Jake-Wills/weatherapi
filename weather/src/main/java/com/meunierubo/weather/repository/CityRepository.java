package com.meunierubo.weather.repository;

import dto.weatherapi.City;
import jakarta.inject.Inject;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

@Component
public class CityRepository {

    private final static String SQL_INSERT_CITY = "INSERT INTO CITIES VALUES (:id, :name, :postcode, :region, :country)";
    private final static String SQL_Delete_CITY = "DELETE FROM CITIES WHERE ID = :id";
    private final static String SQL_GET_CITIES = "SELECT * FROM CITIES";

    private final static String SQL_UPDATE_CITY = "UPDATE CITIES SET NAME = :name, POSTCODE=:postcode,REGION=:region,COUNTRY=:country where ID=:id";

    private final static String SQL_GET_CITY = "SELECT * FROM CITIES where ID=:id";
    @Inject
    private NamedParameterJdbcTemplate jdbcTemplate;
    public void createCity(City city){

        var params = new HashMap<String, String>();
        params.put("id", city.getId());
        params.put("name", city.getName());
        //params.put("postcode", city.getPosteCode());
        params.put("region", city.getRegion());
        params.put("country", city.getCountry());
        this.jdbcTemplate.update(SQL_INSERT_CITY, params);

    }

    public void deleteCity(String id){
        var params = new HashMap<String, String>();
        params.put("id",id);
        this.jdbcTemplate.update(SQL_Delete_CITY, params);
    }

    public List<City>getCities(){

        List<City> res = this.jdbcTemplate.query(SQL_GET_CITIES,new HashMap<String, String>(),(resultSet, rowNum) -> {
            City city = new City();
            city.setId(resultSet.getString("ID"));
            city.setName((resultSet.getString("NAME")));
         //   city.setPosteCode(resultSet.getString("POSTCODE"));
            city.setRegion(resultSet.getString("REGION"));
            city.setCountry(resultSet.getString("COUNTRY"));

            return city;
        });
        return null;
    }

    public Object getCity(String id) throws EmptyResultDataAccessException {
        var params = new HashMap<String, String>();
        params.put("id", id);
        Map<String,Object> res = this.jdbcTemplate.queryForMap(SQL_GET_CITY,params);
        return res;
    }

    public void updateCity(String id,City city){

        var params = new HashMap<String, String>();
        params.put("id", id);
        params.put("name", city.getName());
       // params.put("postcode", city.getPosteCode());
        params.put("region", city.getRegion());
        params.put("country", city.getCountry());
        this.jdbcTemplate.update(SQL_UPDATE_CITY, params);

    }
}
