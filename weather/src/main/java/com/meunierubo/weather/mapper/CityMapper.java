package com.meunierubo.weather.mapper;

import dto.weatherapi.City;
import com.meunierubo.weather.entity.CityEntity;
import org.springframework.stereotype.Component;

public class CityMapper {

    public static City toDto(CityEntity entity) {
        City dto = new City();
        dto.setId(entity.getId());
        dto.setName(entity.getName());
      //  dto.setPosteCode(entity.getPostcode());
        dto.setRegion(entity.getRegion());
        dto.setCountry(entity.getCountry());

        return dto;
    }

    public static CityEntity toEntity(City city) {
        CityEntity entity = new CityEntity();
        entity.setId(city.getId());
        entity.setName(city.getName());
    //    entity.setPostcode(city.getPosteCode());
        entity.setRegion(city.getRegion());
        entity.setCountry(city.getCountry());

        return entity;
    }
}
