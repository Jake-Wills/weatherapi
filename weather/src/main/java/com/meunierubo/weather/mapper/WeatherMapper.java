package com.meunierubo.weather.mapper;

import com.meunierubo.weather.entity.EnumPrevisionState;
import com.meunierubo.weather.entity.PrevisionEntity;
import dto.openWeatherClient.Model200;
import dto.openWeatherClient.Rain;
import dto.weatherapi.Prevision;

import java.time.Instant;
import java.time.LocalDate;

public class WeatherMapper {
    public static PrevisionEntity toEntity(Model200 dto){
        PrevisionEntity r = new PrevisionEntity();
        r.setCity(dto.getName());
        r.setDate(Instant.now());
        r.setTemperature(dto.getMain().getTemp().floatValue() );
        r.setState(EnumPrevisionState.NUAGEUX);
       // if(dto.getWeather().get(0) instanceof Rain);
        return r;

    }

    public static Prevision todto(PrevisionEntity entity){
        Prevision p = new Prevision();
        p.setCity(entity.getCity());
        p.setDate(LocalDate.now());
        p.setTemperature((int)entity.getTemperature());
        return  p;
    }

}
