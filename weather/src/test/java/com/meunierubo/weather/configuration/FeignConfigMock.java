package com.meunierubo.weather.configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.meunierubo.weather.repository.client.OpenWeatherClient;
import com.meunierubo.weather.repository.interceptors.OpenWeatherInterceptor;
import feign.Feign;
import feign.Logger;
import feign.jackson.JacksonDecoder;
import feign.jackson.JacksonEncoder;
import feign.okhttp.OkHttpClient;
import jakarta.inject.Inject;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import java.util.concurrent.TimeUnit;

import static org.mockito.Mockito.mock;

@Configuration
@Profile("TEST")
public class FeignConfigMock {

    @Bean
    OpenWeatherClient getOpenWheaterClient(){
        return mock(OpenWeatherClient.class);
    }

}
