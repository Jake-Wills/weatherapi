package com.meunierubo.weather.controller;

import dto.weatherapi.City;
import jakarta.ws.rs.core.MediaType;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;


import static io.restassured.RestAssured.given;

public class CityControllerTest extends ControllerTest{

    @Test
    @DisplayName("Test creation city then 200")
    public void testCreationCityOK(){
        var city = new City();
        city.setPosteCode(29200);
        city.setRegion("Finistere");
        city.setName("Brest");
        city.setId("Brest");

        given().headers(this.headesMock())
                .contentType(MediaType.APPLICATION_JSON)
                .body(city)
                .post("/cities")
                .then()
                .statusCode(HttpStatus.SC_OK);
    }

    @Test
    @DisplayName("Test creation city without headers then 403")
    public void testCreationCity403(){
        var city = new City();
        city.setPosteCode(29200);
        city.setRegion("Finistere");
        city.setName("Brest");
        city.setId("Brest");

        given()
                .contentType(MediaType.APPLICATION_JSON)
                .body(city)
                .post("/cities")
                .then()
                .statusCode(403);
    }

    @Test
    @DisplayName("Test creation city without headers then 200")
    public void testDeleteCity(){


        given()
                .contentType(MediaType.APPLICATION_JSON)
                .delete("/cities/brest")
                .then()
                .statusCode(HttpStatus.SC_OK);
    }


}
