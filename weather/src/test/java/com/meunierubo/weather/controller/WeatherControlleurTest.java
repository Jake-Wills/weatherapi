package com.meunierubo.weather.controller;

import com.meunierubo.weather.repository.client.OpenWeatherClient;
import dto.openWeatherClient.Main;
import dto.openWeatherClient.Model200;
import jakarta.inject.Inject;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static io.restassured.RestAssured.given;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;


public class WeatherControlleurTest extends ControllerTest{

    @Inject
    private OpenWeatherClient openWeatherClient;


    @Test
    @DisplayName("Test previsions city then 200")
    public void testCreationCityOK(){
        var model200 = new Model200();
        var main = new Main();
        main.setTemp(new BigDecimal(450));
        model200.setName("brest");
        model200.setMain(main);

when(openWeatherClient.getWeather(any(), any())).thenReturn(model200);
        given().headers(this.headesAdminMock())
                .get("/previsions/Brest")
                .then()
                .statusCode(HttpStatus.SC_OK);
    }


}
